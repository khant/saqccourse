{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "684f2b09",
   "metadata": {},
   "source": [
    "# 0. About this Document\n",
    "\n",
    "* This Jupyter Notebook is intended to lead through the necessary steps of quality control with SaQC\n",
    "* It comes with a real world dataset available [here](https://git.ufz.de/rdm/saqccourse/-/blob/main/data/SoilMoistureData.csv)\n",
    "* Along the lines of the quality control setup, we will cover the most import aspect and functionalities of SaQC and its Python API"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cbe40a0",
   "metadata": {},
   "source": [
    "# 1. Data import and preparation\n",
    "\n",
    "* SaQC does not provide data input/output functionality\n",
    "* Data import and preparation is done using the [pandas](https://pandas.pydata.org/) package\n",
    "* `SaQC` accepts (one or more) [`pandas.DataFrame`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) as input"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce17f929",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "data = pd.read_csv('data/SoilMoistureData.csv')\n",
    "\n",
    "# lets see how the first entries and the heading of the data looks like:\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42ff4358",
   "metadata": {},
   "source": [
    "* `SaQC` expects a `pd.DataFrame` with an index of type [`pandas.DatetimeIndex`](https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.html#pandas.DatetimeIndex)\n",
    "* When reading data from CSV files, the index and its data type usually need to be set explicity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a922110",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# set a new index column\n",
    "data = data.set_index('Date Time')\n",
    "# cast the new index to a datetime data type\n",
    "data.index = pd.DatetimeIndex(data.index)\n",
    "# check out the leading entries\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90e11417",
   "metadata": {},
   "source": [
    "# 2. Initializing SaQC\n",
    "\n",
    "To get things rolling, we need to:\n",
    "1. Import the package `saqc`\n",
    "2. Instantiate a [SaQC](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC) object with a prepared data set\n",
    "3. Assign the generated `SaQC` object to a variable (here called `qc`) for later usage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12c6d9f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import saqc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f710f92d",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = saqc.SaQC(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "069bba87",
   "metadata": {},
   "source": [
    "The initialized object holds two data structures:\n",
    "   1. the `data` itself\n",
    "   2. the state of quality `flags` for every datapoint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9371e8a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6114a672",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.flags"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e9eba27",
   "metadata": {},
   "source": [
    "At this stage, all the flags evaluate to `-inf`, which is the representation of the status `UNFLAGGED`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "301bf554",
   "metadata": {},
   "source": [
    "# 3. Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0f33c05",
   "metadata": {},
   "source": [
    "* SaQC provides the plotting routine [`plot`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.plot)\n",
    "* The plots show the data, existing flags and name their origin"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86fd2cc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# this plots the soil moisture measurements\n",
    "qc.plot('Box17_Moist6 [%]')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b15a290",
   "metadata": {},
   "outputs": [],
   "source": [
    "# this plots the battery voltage\n",
    "qc.plot('Box17_Voltage [mV]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec50f860",
   "metadata": {},
   "source": [
    "To generate zoomed plots, adjust the time range to plot using the `xscope` keyword of the [`plot`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.plot) method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a39ec0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot('Box17_Moist6 [%]', xscope='2021-03')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36939d0d",
   "metadata": {},
   "source": [
    "# 4. The Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ed0a328",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe4479ff",
   "metadata": {},
   "source": [
    "* The dataset we use during this course holds the data from two soil moisture sensors and the voltage data of the associated data logger:\n",
    "  1. `'Box17_Moist6 [%]'`\n",
    "  2. `'Box17_Voltage [mV]'`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c858035d",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot(\"Box17_Moist6 [%]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d412b844",
   "metadata": {},
   "source": [
    "* SoilMoisture contains anomalies, looking at the plots we can visually identify:\n",
    "  * implausible values (percentages below zero and above 100)\n",
    "  * outliers/spikes\n",
    "  * constant value courses\n",
    "  * noise"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91669061",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot(\"Box17_Voltage [mV]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f699f0d5",
   "metadata": {},
   "source": [
    "* Voltage:\n",
    "  * Outliers\n",
    "  * Decreasing towards the end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aed14658",
   "metadata": {},
   "source": [
    "# 5. Applying a basic range test"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cae46a20",
   "metadata": {},
   "source": [
    "* The simple cutoff test [`flagRange`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.flagRange) sets flags where the data exceeds certain upper and/or lower bounds\n",
    "* These bounds are variable and controlled by the function's parameter `min` and `max`\n",
    "* The application of the method returns a new `SaQC` object, that reflects the result of the method's application\n",
    "* The original object remains unchanged"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "281ea6b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.flagRange(field='Box17_Voltage [mV]', min=3000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e20a2c62",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot('Box17_Voltage [mV]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5be189e",
   "metadata": {},
   "source": [
    "* All values lower than *3000* mV are flagged\n",
    "* The flags are associated with the [`flagRange`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.flagRange) test"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df26d4f3",
   "metadata": {},
   "source": [
    "# 6. Flags / Quality Labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60ef2f62",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.flags"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d9c871b",
   "metadata": {},
   "source": [
    "* The last entries of the battery variable now evaluate to the floating point value `255`\n",
    "* All other flags evaluate to `-inf`\n",
    "\n",
    "## 6.1 Special Flags\n",
    "\n",
    "* Some flags carry special semantics:\n",
    "  * `255`: Marks values as 'bad', i.e. those values failed to pass at least one test\n",
    "  * `-inf`: Represents the status unflagged. Values marked as `-inf` where either never checked at all *or* did pass all tests so far\n",
    "* For a more convient usage of these special flags SaQC provides the following alliases:\n",
    "  * `255`: `BAD`\n",
    "  * `-inf`: `UNFLAGGED`\n",
    "* To use these aliases import them from the main `saqc` package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25392b28",
   "metadata": {},
   "outputs": [],
   "source": [
    "from saqc import BAD, UNFLAGGED"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "990cf7b4",
   "metadata": {},
   "source": [
    "## 6.2 Setting specific flags\n",
    "\n",
    "* All functions support the paramater `flag` to set a custom flag value\n",
    "* By defult all values between `0` and `255` are available"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b17dd7e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc1 = qc.flagRange('Box17_Voltage [mV]', min=3100, flag=25)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e068126f",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc1.flags.loc[\"2021-03-17\", 'Box17_Voltage [mV]']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72b3bc1d",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc1.data.loc[\"2021-03-17\", 'Box17_Voltage [mV]']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fbd6558",
   "metadata": {},
   "source": [
    "## 6.3 Flagging Schemes\n",
    "\n",
    "* Per default SaQC supports flags between `0` and `255`, plus the unflagged marker `-inf`\n",
    "* This scheme is no hardcoded and can be changed\n",
    "* SaQC currently supports the following flagging schemes:\n",
    "  * `\"float\"`: The default scheme\n",
    "  * `\"simple\"`: Supports the string flags `\"UNFLAGGED\"`, `\"BAD\"`, `\"OK\"`\n",
    "  * `\"dmp\"`: Compatible to the flagging scheme of the UFZ Datamanagement Portal\n",
    "  * `\"positional\"`: Ask Corinna Rebmann for details :-) \n",
    "* To set a specific flagging scheme use the `scheme` parameter when initializing a new `SaQC` object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "edd6f981",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc_simple = saqc.SaQC(data, scheme=\"simple\")\n",
    "qc_simple = qc_simple.flagRange('Box17_Voltage [mV]', min=3000)\n",
    "qc_simple.flags['Box17_Voltage [mV]']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ccceaae9",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc_dmp = saqc.SaQC(data, scheme=\"dmp\")\n",
    "qc_dmp = qc_dmp.flagRange('Box17_Voltage [mV]', min=3000)\n",
    "qc_dmp.flags['Box17_Voltage [mV]']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ad5cc45",
   "metadata": {},
   "source": [
    "# 7. Transferring flags between variables\n",
    "\n",
    "* We want to transfer quality information from one variable to another (i.e. if the battery voltage is below a certain threshold, flag the soilmoisture)\n",
    "* As both variables share the same index, we can easily do that with the [`transferFlags`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.transferFlags) method \n",
    "* We will also assign a [`label`](https://rdm-software.pages.ufz.de/saqc/documentation/GlobalKeywords.html#label-keyword) to this flag for an easy distinguishing of the flag's origins"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c5a3306",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "qc = qc.transferFlags(field='Box17_Voltage [mV]', target='Box17_Moist6 [%]', label='battery voltage not trustworthy')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "800b2fb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# we briefly check out the results:\n",
    "qc.plot('Box17_Moist6 [%]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a73f28c",
   "metadata": {},
   "source": [
    "* The battery voltage explains some, but very few of the anomalies here\n",
    "* Note, how the plot's legend shows the `label` string we assigned  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c7ab24e",
   "metadata": {},
   "source": [
    "# 8. Fields and targets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90f207ec",
   "metadata": {},
   "source": [
    "* The field a SaQC function operates on, is not necessarily the field it writes the results to\n",
    "* All function support the parameter `target` which determines to which variable SaQC will write to\n",
    "* Per default `field = target`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fce64b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc_target = saqc.SaQC(data).flagRange(field='Box17_Voltage [mV]', target='test', min=3000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd99c882",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc_target.data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cec2e358",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc_target.flags"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3befeb72",
   "metadata": {},
   "source": [
    "# 9. Apply custom conditions\n",
    "\n",
    "* As a variable of unit *%*, soil moisture values should be in the range [0, 100]. So we could apply another range test like:\n",
    "  ```python\n",
    "  qc = qc.flagRange('Box17_Moist6 [%]', min=0, max=100)\n",
    "  ```\n",
    "* We can reproduce the very same functionality with the more general [`flagGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.flagGeneric) function. \n",
    "* [`flagGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.flagGeneric) allows to define custom flagging conditions, that can depend on any variables hold by the [`SaQC`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC) object\n",
    "* The formulation of the above condition ('flag everything outside the range of [0, 100]') in standard python notation could look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67d21591",
   "metadata": {},
   "outputs": [],
   "source": [
    "condition = lambda x: (x < 0) | (x > 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "824ac676",
   "metadata": {},
   "source": [
    "* Pass this condition to the `func` parameter of [`flagGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.SaQC.html#saqc.SaQC.flagGeneric)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45990381",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.flagGeneric('Box17_Moist6 [%]', func=condition, label='data out of range')\n",
    "qc.plot('Box17_Moist6 [%]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "626261a0",
   "metadata": {},
   "source": [
    "# 10. Filtering flagged values\n",
    "\n",
    "* Flagged values will **not** be passed to suceeding tests\n",
    "* We refer to this behaviour as 'masking'\n",
    "* Technically all values equal or higher a certain flag threshold will be replaced by `NaN` before a test function is executed. After a tests finishes, the introduced `NaN` values are replaced again by their original values\n",
    "\n",
    "* By default the masking threshold is `BAD` (i.e. `255`), however this can be changed\n",
    "* All function support the [`dfilter`](https://rdm-software.pages.ufz.de/saqc/documentation/GlobalKeywords.html#dfilter-and-flag-keywords) parameter that determines which of the flagged values are passed to the next test and which are not\n",
    "* This is quite convenient to remove flagged values from a plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adf5492b",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot('Box17_Moist6 [%]', dfilter=BAD)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e6149e1",
   "metadata": {},
   "source": [
    "# 11. Generative data products with rolling windows\n",
    "\n",
    "* Data is obviously still noisy -> one might want to limit data noise\n",
    "* Therefor we generate a measure of the data noise\n",
    "* easiest way: measure the standart deviation (\"scattering\") of the data in small windows of fixed size\n",
    "\n",
    "Therefor we need to define a function, that measures the standart deviation of the data in small window portions in standart python notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5845c11c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def rolling_standart_deviation(x):\n",
    "    return x.rolling(window='1D', center=True).std()\n",
    "\n",
    "# Or, alternatively in lambda notation this would be:\n",
    "\n",
    "rolling_standart_deviation = lambda x: x.rolling(window='1D', center=True).std()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac131512",
   "metadata": {},
   "source": [
    "Second, we apply the function to the data via the [`processGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.processGeneric) method.\n",
    "* It can apply arbitrary functions onto the data (parameter: *func*)\n",
    "* The result is stored to a new data field (parameter: *target*)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a6f37fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.processGeneric('Box17_Moist6 [%]', target='rolled_1D_std', func=lambda x: x.rolling('1D', center=True).std())\n",
    "qc.plot('rolled_1D_std')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93ac7cde",
   "metadata": {},
   "source": [
    "* the new variable *'rolled_1D_std'* correlates with the local scatteredness (with regard to a one day window)\n",
    "* the variable can be thought of as denoting the *mean deviation from the one day mean*\n",
    "* Lets assume we want to limit this deviation by *2 %*\n",
    "* the steps necessary equal the steps that where to be taken for limiting the voltage \n",
    "\n",
    "However, another way to achieve this, is by using the [`flagGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.flagGeneric) function. Now we use the Soilmoisture measurements as the `target`:\n",
    "* as target we pass it the target variable: *'Box17_Moist6 [%]'* (parameter:*target*)\n",
    "* as condition we pass it a function, that returns TRUE, whenever the variable it is applied on, exceeds the value 2 (parameter:*func*)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29f76f87",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.flagGeneric('rolled_1D_std', target='Box17_Moist6 [%]', func=lambda x: x>2, label='data is too much scattered')\n",
    "qc.plot('Box17_Moist6 [%]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0cff764c",
   "metadata": {},
   "source": [
    "# 12. Leftovers and Isolated points\n",
    "\n",
    "* After the application of statistic basad data filter, singleton values or groups of isolated values may pass the flagging\n",
    "* In the above example, the variance may have dropped for one day below 2, even in the noisy part of the data, but one may not trust such data groups that are isolated by invalid data only.\n",
    "\n",
    "Checking out the result cleared from flagged data reveals, there is one isolated data point left:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97c89781",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot('Box17_Moist6 [%]', dfilter=BAD)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5244b75f",
   "metadata": {},
   "source": [
    "* Catching isolated values may be achieved via [`flagIsolated`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.flagIsolated) function\n",
    "* We apply the condition, that we dont trust data that doesnt range over more than an our and is isolated by gaps bigger than two hours, with the following parametrisation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b42c3d54",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.flagIsolated('Box17_Moist6 [%]', group_window='1h', gap_window='2h')\n",
    "qc.plot('Box17_Moist6 [%]', dfilter=BAD)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42f8cd80",
   "metadata": {},
   "source": [
    "# 13 Projecting data to regular frequency grids\n",
    "\n",
    "* more elaborated (statistical) data analytics requires the data to be sampled at an uniform frequency.\n",
    "\n",
    "Checking out the soilmoistures data (index):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "479636f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.data['Box17_Moist6 [%]'].head(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1f58f5c",
   "metadata": {},
   "source": [
    "* The data seems intended to be collected at a 15 minutes sampling rate, but to jitter around the right mark in a certain range.\n",
    "* To obtain a plausible representation of the data at regular frequency grid, an idea could be to assign any strict index multiple of 15 Minutes the nearest value available (in a 15 minutes range)\n",
    "* This can be achieved with the [`shift`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.shift) resampler:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03f0c144",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.shift('Box17_Moist6 [%]', freq='15min', target='regular')\n",
    "qc.data['regular'].head(20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "613a0d9e",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc.plot('regular')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02f393f1",
   "metadata": {},
   "source": [
    "# 14. Outlier Detection\n",
    "\n",
    "* There is no universal approach for the detection of (univariate) outliers in variables that are not stationary in mean and variance\n",
    "* Best methods may vary dending on the distribution of the target variable\n",
    "* Usually one tries to derive a variable (score) that itself is \"stationary\" in mean and variance and apply the 3 sigma rule on its standardisation\n",
    "* Unfortunately, a derivation to obtain such a variable is nearly never at hand\n",
    "* Fortunately, in most cases, it may be sufficient if that requirement is met only approximatly or locally (in a rolling window for example):\n",
    "    \n",
    "\n",
    "# 14.1 Fitting a (filter) model\n",
    "\n",
    "* one way for deriving a stationary variable, is, to fit a smooth model curve to the data \n",
    "* one might configure that fit, so that it is capable of seperating the data signal from its noise.\n",
    "    * the smoothing should be \"elastic\" enough to follow the actual trend of the data\n",
    "    * then the difference between data and smoothing (the residuals), only should contain the noise, wich one can assume to be (at least locally and approximately) stationary\n",
    "    * then one can (in theory) apply the quite simple 3-sigma rule to the data, to identify data deviations, that may not be produced by this back ground noise. Those points then might be qualified outliers.\n",
    "* smoothing methods to be applied, may be:\n",
    "    * rolling *first moments* (rolling mean, rolling median)\n",
    "    * polynomial fits\n",
    "    * frequency domain filters\n",
    "\n",
    "Lets try a frequency domain filter [`fitLowpassFilter`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.fitLowpassFilter). \n",
    "\n",
    "* It works by transforming the data into its frequency representation, and then, removing any energy above a cutoff frequency (parameter: *cutoff*). \n",
    "* So: only the \"low\" frequencies pass. this makes sence, since the noise is expected to be represented by the highest frequencies in the signal.\n",
    "* one can obtain the *residual* signal (the noise) by subtracting the filter result from the original variable. Arithmatic Operations on variables can be performed with [`processGeneric`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.processGeneric). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c40d3fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.fitLowpassFilter('regular', target='filtered', cutoff='2D')\n",
    "qc = qc.processGeneric(field=['regular', 'filtered'], target='residuals', func=lambda x,y: x-y)\n",
    "qc.plot('residuals')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b0d5fc4",
   "metadata": {},
   "source": [
    "# 14.2 Applying classic 3 Sigma rule\n",
    "\n",
    "* Obveously the data is not really stationary\n",
    "* But maybe it is stationary enough to clearly seperate the malicous data\n",
    "* the function [`flagZScore`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.flagZScore) applies the 3 sigma rule to the data. This rule states, that samples that are distanted from the data mean (~0) by more than three times its standard deviation, can be assumed to not be drawn from the underlying distribution by a probability of above 99 prercent (and thus can be regarded outliers with that probability)\n",
    "\n",
    "We flag the residuals and then transfer those flags onto the original variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7daebe1e",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.flagZScore('residuals', thresh=3)\n",
    "qc = qc.transferFlags('residuals', target='regular', label='Zscore above 3')\n",
    "qc.plot('regular')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f894d24",
   "metadata": {},
   "source": [
    "# 15. Projection of flags between differently sampled variables\n",
    "\n",
    "* Finally, the flagging result obtained from the regularly sampled (shifted) version of the data should get projected onto the original in a plausible way.\n",
    "* This can be achieved with the [`concatFlags`](https://rdm-software.pages.ufz.de/saqc/_api/saqc.core.SaQC.html#saqc.core.SaQC.concatFlags) function. It tries to invert the projection used for generating the regularly sampled data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "674e7006",
   "metadata": {},
   "outputs": [],
   "source": [
    "qc = qc.concatFlags('regular', 'Box17_Moist6 [%]', freq='15min', method='inverse_nshift')\n",
    "qc.plot('Box17_Moist6 [%]')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
