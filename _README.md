# The SaQC Course

Try out `SaQC` yourself, we've already installed all prerequisites...

Helpful links
- Documentation: https://rdm-software.pages.ufz.de/saqc/
- Repository: https://git.ufz.de/rdm-software/saqc
- PyPi: https://pypi.org/project/saqc/
